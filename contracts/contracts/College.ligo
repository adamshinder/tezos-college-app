//type id = int;
//type storage = map (id, applicant);

type applicant = {
name : string,
satscore : int,
gpa : int,
id: id
}; // creates a record of applicants

//const collegeBoard : address = ("tz1SVKrTXr7UPRZZKQY3Gudx1QWs1BKaDpkF": address);

type action =
| Addname (string)
| Addsatscore (int)
| Addgpa (int)
| AddApplicant (applicant)
//| UpdateContract(applicant) // Do I need this function? 


let addname = ((n, name): (applicant, string)) : applicant => {...n, name: name};
let addsatscore = ((s, score): (applicant, int)) : applicant => {...s, satscore: score};
let addgpa = ((s, gpa): (applicant, int)) : applicant => {...s, gpa: gpa};
//let Addapplicant = (i : storage) : storage => Map.add ((applicant : record [0]), i);
(**
let Addapplicant = ((s, info): (storage, (id, applicant))): storage => { //this returns a storage because ": storage" 
  switch (Map.find_opt(info[0], s)) { //info [0] is id because first of the tuple  //the result of the find_opt can be Some or None
    | None => Map.add(info[0], info[1], s //change the storage //Map.add adds a value and returns the map //add info[1] in the place of info [0] info1 = applicant
    | Some (acc) =>  {...s, storage: Map.update(info[0], Some (info[1]), s)} //if an applicant is found, update the whole thing
  };
};
*)
//it's a map of ID's to applications 

let main = ((p,applicant): (action, applicant)) => { //give the main a name of string, store it in the applicant and return the record with a name inside it 
  let applicant =
    switch (p) {
        | Addname (t) => addname((applicant, t))
        | Addsatscore (t) => addsatscore((applicant, t))
        | Addgpa (t) => addgpa((applicant,t))
        | AddApplicant (t) => ((Addapplicant, t))
      //  | Submit (t)
    };
  ([]: list(operation), applicant); //returns a list of all operations and new storage. //functional programming makes new values, doesn't change them
};
//A contract is a function with a parameter and the storage